#!/usr/bin/env python
from icalendar import Calendar, Event
from datetime import datetime, timedelta
import tempfile, os, re, csv, socket, sys, argparse


def main(argv):
    args = parse_args(argv)
    instart = datetime.strptime(args.start, '%x')
    day = instart.day
    month = instart.month
    year = instart.year
    icalendarize(args.incsv, args.out, args.domain, day, month, year)
    
def parse_args(args):
    locale_date_format = datetime.now().strftime('%x') # what locale +%x gives.
    domain = socket.getfqdn()
    parser = argparse.ArgumentParser(description="Convert csv training planning to ical.\
                                     For example: quick-plan-ical.py  -i training.csv -o training.ics -s"  + locale_date_format)
    parser.add_argument("-i", "--incsv", type=str, help="source csv file", required=True)
    parser.add_argument("-o", "--out", type=str, help="destination ical file. If not there will print to stdout")
    parser.add_argument("-s", "--start", type=str, help="date that will be used as scheduling start, it will \
                        be used to determine the week and then the monday of this week will be used as start.\
                        The date format follows the way is defined in locale, currently: " + locale_date_format, required=True)
    parser.add_argument("-d", "--domain", type=str, help="the domain to create the uid of each event", default=domain)
    return parser.parse_args(args)


'''
Gets the summary from summaries or insert to summaries catalog.
Summary must be in format:
running: SpeedWorkout A
- warmup: 5:00 @z1
- run: 10:00 @z2
- repeat: 6
  - run: 2:00 @z4
    - run: 4:00 @z2
    - run: 5:00 @z2
    - recover: 5:00
or:
    SpeedWorkout A
If single line looks for a declaration done before, if not existing use as simple text(not reused-reusable)

@param str summary
    the summary to parse
@param dict summaries
    the summaries dictionary
@returns array 
    with [summary, description]
'''
def getSummary(summary, summaries):
    if "\n" in summary:
        #search if summary has any ":" if is it it's a declaration
        pattern = re.compile(".*:.*")
        if pattern.match(summary):
            #separate first line
            first_line = summary.split('\n', 1)[0]
            #set the activity taking first declaration left key
            activity = re.split(":", first_line)
            # set dict key as whole summary
            summaries[activity[1].strip()] = summary
            return [activity[1].strip(), summary]
    if summary in summaries:
        return [summary, summaries[summary]]
    else:
        return [summary,summary]

def icalendarize(inputfile, outputfile, domain, day, month, year):
    summaries = dict()
    #datetime(year, month, day, hour=0, minute=0, second=0, microsecond=0, tzinfo=None)
    start = datetime(year, month, day, 0, 0, 0)
    wd = start.weekday()
    #Force start on monday (so really user chooses only the start week)
    if wd != 0 :
        print("CSV Plan must start on monday, so planning will start at the specified week but different day: " + str(day-wd), file=sys.stderr)
        start = datetime(year, month, day-wd, 0, 0, 0)
    cal = Calendar()
    cal.add('prodid', '-//communia.org//NONSGML Quick Plan Ical 1.0//')
    cal.add('version', '2.0')

    with open(inputfile,  newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for week,row in enumerate(reader):
            for weekday,day in enumerate(row):
                if weekday==0 or row[day] == '':
                    continue
                summary = getSummary(row[day], summaries)
                event = Event()

                event.add('dtstamp', datetime.now())
                event.add('uid', str(datetime.now()) + "//@" + domain )
                event.add('dtstart', start.date() + timedelta(days=weekday-1, weeks=int(row['WEEK'])-1))
                event.add('summary', summary[0])
                event.add('description', summary[1])
                event.add('transp', 'TRANSPARENT')
                event.add('categories', ['training'])
                cal.add_component(event)

    if (outputfile == None ):
        print(cal.to_ical())
    else:
        f = open(os.path.join(outputfile), 'wb')
        f.write(cal.to_ical())
        f.close()
        print('saved to ' + outputfile )



if __name__ == "__main__":
   main(sys.argv[1:])