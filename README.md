# quick-plan-ical

Python command line tool to import, schedule and share workouts i.e. weekly based training plans, to calendar using ical specification: https://www.ietf.org/rfc/rfc5545.txt .


It is vastly inspired by the tool quick-plan by mgifos ( https://github.com/mgifos/ ), but is much more simplier because it hasn't to translate from workout definition notation to garmin workouts. Instead it will translate the declaration title to event's summary and what is workout definition to description.


The source csv file format to use is exactly the same as proposed in quick-plan, but less strict because there's no description parsing.


So every event must be declared as this:

```
running: run fast
- warmup: 10:00 @ z2
- repeat: 3
  - run: 1.5km @ 5:10-4:40
  - recover: 500m @ z2
- cooldown: 05:00
```
But differs from original quick-plan because it will not try to parse the text of the description so it can be also:
```
running: run fast
Awesome and very fast run
```
Future uses will be just referenced with:
```
run fast
```

## Usage
```
usage: plan-to-csv.py [-h] -i INCSV [-o OUT] -s START [-d DOMAIN]

Convert csv training planning to ical. For example: quick-plan-ical.py -i      
training.csv -o training.ics -s10/18/18

optional arguments:
  -h, --help            show this help message and exit
  -i INCSV, --incsv INCSV
                        source csv file
  -o OUT, --out OUT     destination ical file
  -s START, --start START
                        date that will be used as scheduling start, it will be 
                        used to determine the week and then the monday of this 
                        week will be used as start. The date format follows    
                        the way is defined in locale, currently: 10/18/18      
  -d DOMAIN, --domain DOMAIN
                        the domain to create the uid of each event
```